# TypeScript React Boilerplate

## Available Scripts

`npm run build`

Build application

`npm start`

Runs app in the development mode
http://localhost:8080

## Run

```
docker build -f ./config/Dockerfile -t app  .
docker run -p 80:80 app:latest
```

## Docker compose
```
docker-compose -f ./config/docker-compose.yml up --build
```

## Styleguide

```
docker build -f ./docs/Dockerfile -t styleguide  .
docker run -p 80:80 styleguide:latest
```
