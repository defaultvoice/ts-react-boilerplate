/* eslint-disable */
const path = require('path')
// const fs = require("fs");

module.exports = {
  styleguideDir: path.resolve(__dirname, 'styleguide'),
  pagePerSection: true,
  sections: [
    {
      name: 'UI Components',
      content: 'docs/ui.md',
      components: 'src/ui/**/*.{js,jsx,ts,tsx}',
      exampleMode: 'expand', // 'hide' | 'collapse' | 'expand'
      usageMode: 'expand', // 'hide' | 'collapse' | 'expand'
      sectionDepth: 3,
    },
  ],
  propsParser: require('react-docgen-typescript').withDefaultConfig({
    savePropValueAsString: true,
  }).parse,
  webpackConfig: Object.assign({}, require('./webpack.config.js'), {}),
  skipComponentsWithoutExample: true,
  theme: {
    maxWidth: 'inherit',
  },
  styles: {
    StyleGuide: {
      '@global': {
        ':root': {
          /* Text */
          '--white': '#fff',
          '--black': '#222',
          '--main-text': '#747A81',
          '--icon': '#9EA2A7',
          '--separator': '#E6E6E6',
          '--dark-separator': '#D2D4D9',
          '--hover-block': '#F7F7F7',
          '--i-have-no-idea-what-color-is-it': '#B9BCC0',
          /* Link */
          '--button-color': '#1F91EF',
          '--text-link-color': '#1F91EF',
          '--button-hover': '#0683EA',
          /* Accent */
          '--red': '#FD5461',
          '--green': '#38BA51',
          '--yellow': '#FFD22D',
          fontSize: 14,
          fontFamily: 'Roboto, sans-serif',
          color: 'var(--black)',
        },
        '*, *::before, *::after': {
          boxSizing: 'border-box',
          outline: 'none' /* FIXME: nice a11y */,
        },
        button: {
          cursor: 'pointer',
          border: 'none',
          background: 'none',
          padding: 0,
        },
        'h1,h2,h3,h4,h5,h6': {
          margin: 0,
        },
        menu: {
          margin: 0,
          padding: 0,
        },
      },
    },
  },
}
