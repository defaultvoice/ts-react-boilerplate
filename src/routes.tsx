import * as React from 'react'

import { Switch, Route } from 'react-router-dom'

import { MainPage } from './pages'

const routes = [
  {
    key: 'main',
    path: '/',
    exact: true,
    component: MainPage,
  },
]

export const Routes = (): React.ReactElement => {
  return (
    <Switch>
      {routes.map(({ key, component, path, exact }) => (
        <Route key={key} exact={exact} path={path} component={component} />
      ))}
    </Switch>
  )
}
