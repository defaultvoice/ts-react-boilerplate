import styled from 'styled-components';

export const Text = styled.div`
  font-size: 2rem;
  line-height: 1.3;
  color: ${({ color }): string => color || 'var(--main-text)'};
`
