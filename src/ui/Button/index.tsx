import styled from 'styled-components'

export const Button = styled.button`
  display: inline-block;
  border-radius: 8px;
  color: ${(props): string => props.color || 'var(--white)'};
  background: ${(props): string  => props.background || 'var(--button-color)'};
  margin: ${(props): string  => props.margin || '0'};
  font-weight: 500;
  transition: background 300ms;
  padding: ${({ padding }): string => padding || '14px 12px'};
  text-decoration: none;
  min-width: 100px;
  white-space: nowrap;
  margin: 0 10px;
  text-align: center;

  :hover {
    background: ${(props): string => props.background || 'var(--button-hover)'};
    color: ${(props): string => props.hoverColor || 'var(--white)'};
  }

  :disabled {
    cursor: default;
    background: rgba(31, 145, 239, 0.3);
  }
`
