import * as React from 'react'

import { Greetings } from '../../features/Greetings'

export const MainPage: React.FC = () => {
  return (
    <Greetings />
  )
}
