import { createStore, createEvent } from 'effector'

export const $counter = createStore(0);

export const incremented = createEvent();
export const decremented = createEvent();

$counter
  .on(incremented, (state) => state + 1)
  .on(decremented, (state) => state - 1);
