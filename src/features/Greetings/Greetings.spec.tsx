import * as React from 'react'

import { render, fireEvent, cleanup } from '@testing-library/react'

import { Greetings } from './'

afterEach(cleanup)

describe('Greetings component loads correctly', () => {
  const { container, getByText } = render(<Greetings />)
  const { firstChild } = container
  test('renders correctly', () => {
    expect(getByText('Hello, world!')).toBeInTheDocument()
  })

  test('first child should match snapshot', () => {
    expect(firstChild).toMatchInlineSnapshot(`
      <div
        class="sc-AxjAm jbDFFy"
      >
        Hello, world!
      </div>
    `)
  })
})

describe('State is managed correctly', () => {
  it('count starts at 0', () => {
    const { getByTestId } = render(<Greetings />)
    const count = getByTestId(`counter`)
    expect(count.innerHTML).toBe("0")
  })

  it('should add 1 to count', () => {
    const { getByTestId } = render(<Greetings />)
    const count = getByTestId('counter')
    const add = getByTestId('increment')

    fireEvent.click(add)
    expect(count.innerHTML).toBe("1")
  })

  it('should remove 1 from count', () => {
    const { getByTestId } = render(<Greetings />)
    const count = getByTestId('counter')
    const remove = getByTestId('decrement')

    fireEvent.click(remove)
    expect(count.innerHTML).toBe("0")
  })
})
