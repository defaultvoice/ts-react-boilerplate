import * as React from 'react'

import { useStore } from 'effector-react'

import { Text } from '../../ui/Text'
import { Button } from '../../ui/Button'

import { $counter, incremented, decremented } from './models/counter'

export const Greetings: React.FC = () => {
  const counter = useStore($counter);

  const handleIncrement = () => {
    incremented()
  }

  const handleDecrement = () => {
    decremented()
  }

  return (
    <>
      <Text>Hello, world!</Text>
      <Text data-testid="counter">{counter}</Text>
      <Button data-testid="increment" type="button" onClick={handleIncrement}>+</Button>
      <Button data-testid="decrement" type="button" onClick={handleDecrement}>-</Button>
    </>
  )
}
