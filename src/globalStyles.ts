import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  :root {
    /* Text */
    --white: #fff;
    --black: #222;
    --main-text: #747A81;
    --icon: #9EA2A7;
    --separator: #E6E6E6;
    --hover-block: #F7F7F7;
    /* Link */
    --button-color: #1F91EF;
    --text-link-color: #1F91EF;
    --button-hover: #0683EA;
    /* Accent */
    --red: #FD5461;
    --green: #38BA51;
    --yellow: #FFD22D;
  }

  *, *::before, *::after {
    box-sizing: border-box;
    outline: none;  /* FIXME: nice a11y */
  }

  html {
    font-size: 14px;
  }

  body {
    font-family: 'Roboto', sans-serif;
    color: var(--black);
    background: var(--white);
    overflow-x: hidden;
  }

  button {
    cursor: pointer;
    border: none;
    background: none;
    padding: 0;
  }

  button:disabled {
    cursor: default;
    border: none;
    background: none;
  }

  h1,h2,h3,h4,h5,h6 {
    margin: 0;
  }

  menu {
    margin: 0;
    padding: 0;
  }
`
