import * as React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Normalize } from 'styled-normalize'

import { Routes } from './routes'
import { GlobalStyles } from './globalStyles'

export const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Normalize />
      <GlobalStyles />
      <Routes />
    </BrowserRouter>
  )
}
